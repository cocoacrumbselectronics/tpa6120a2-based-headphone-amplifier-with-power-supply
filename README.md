# TPA6120A2-based-headphone-amplifier-with-power-supply

A headphone amplifier based on the TPA6120A2 from Texas Instruments with 
ultra regulated power supplies (using LT3045 and LT3094 from Analog Devices).

See also the "TPA6120A2 based headphone amplifier" blog post on 
[cocoacrumbs](http:\\www.cocoacrumbs.com).
